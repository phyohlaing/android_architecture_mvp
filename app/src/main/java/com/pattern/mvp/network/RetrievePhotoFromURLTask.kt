package com.pattern.mvp.network

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import java.net.URL

class RetrievePhotoFromURLTask : AsyncTask<String, Unit, Bitmap>(), UrlToPhotoService {

    private var exception: Exception? = null

    private lateinit var callback: RetrievePhotoCallBack

    override fun setRetrievePhotoCallBack(cb: RetrievePhotoCallBack): RetrievePhotoFromURLTask {
        callback = cb
        return this
    }

    override fun retrieve(urlString: String) {
        execute(urlString)
    }

    override fun doInBackground(vararg args: String): Bitmap? {
        val url = URL(args[0])
        return try {
            BitmapFactory.decodeStream(url.openConnection().getInputStream())
        } catch (e: Exception) {
            this.exception = e
            null
        }
    }

    override fun onPostExecute(bitmap: Bitmap?) {
        bitmap?.let {
            callback.onPhotoSuccessfullyRetrieved(it)
        } ?: callback.onFailToRetrievePhoto(exception!!.message)
    }

    interface RetrievePhotoCallBack {
        fun onPhotoSuccessfullyRetrieved(bitmap: Bitmap)
        fun onFailToRetrievePhoto(message: String?)
    }
}