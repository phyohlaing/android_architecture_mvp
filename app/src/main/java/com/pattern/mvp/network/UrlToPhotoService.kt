package com.pattern.mvp.network

interface UrlToPhotoService {
    fun retrieve(urlString: String)
    fun setRetrievePhotoCallBack(cb: RetrievePhotoFromURLTask.RetrievePhotoCallBack): UrlToPhotoService
}