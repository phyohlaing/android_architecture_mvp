package com.pattern.mvp.network

import com.pattern.mvp.model.Photo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

class PhotoService : PhotoRepository {

    private val retrofit = Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()!!

    private val client: PhotoClient

    init {
        client = retrofit.create(PhotoClient::class.java)
    }

    override fun getPhotoFromRemote(callback: PhotoServiceCallback) {

        client.getPhoto().enqueue(object : Callback<Photo> {
            override fun onResponse(call: Call<Photo>?, response: Response<Photo>?) {
                if (response != null && response.isSuccessful) {

                    val photo = response.body()
                    callback.onSuccessPhotoServiceResponse(photo)

                } else {
                    callback.onFailurePhotoServiceResponse("Retrieve photo failed!")
                }

            }

            override fun onFailure(call: Call<Photo>?, t: Throwable?) {
                callback.onFailurePhotoServiceResponse("Retrieve photo failed!")
            }
        })
    }

    interface PhotoServiceCallback {
        fun onSuccessPhotoServiceResponse(photo: Photo?)
        fun onFailurePhotoServiceResponse(message: String)
    }

    interface PhotoClient {
        @GET("photos/1")
        fun getPhoto(): Call<Photo>
    }

}