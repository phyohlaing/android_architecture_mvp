package com.pattern.mvp.network

interface PhotoRepository {
    fun getPhotoFromRemote(callback: PhotoService.PhotoServiceCallback)
}