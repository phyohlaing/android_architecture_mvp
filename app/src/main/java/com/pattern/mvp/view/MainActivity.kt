package com.pattern.mvp.view

import android.graphics.Bitmap
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.pattern.mvp.R
import kotlinx.android.synthetic.main.activity_main.*
import com.pattern.mvp.MVPApplication
import com.pattern.mvp.presenter.MainPresenter

class MainActivity : AppCompatActivity(), MainPresenter.Display {

    private lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initPresenter()
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()
    }

    override fun showErrorMessage(errorMesage: String) {
        Toast.makeText(this, errorMesage, Toast.LENGTH_LONG).show()
    }

    override fun showPhoto(bitmap: Bitmap) {
        photoView.setImageBitmap(bitmap)
    }

    private fun initPresenter() {
        val mvpApp = application as MVPApplication

        presenter = MainPresenter(
                mvpApp.getPhotoService(),
                mvpApp.getUrlToPhotoService())

        presenter.inject(this)
    }

}
