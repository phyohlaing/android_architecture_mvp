package com.pattern.mvp.presenter

import android.graphics.Bitmap
import com.pattern.mvp.model.Photo
import com.pattern.mvp.network.PhotoRepository
import com.pattern.mvp.network.PhotoService
import com.pattern.mvp.network.RetrievePhotoFromURLTask
import com.pattern.mvp.network.UrlToPhotoService

class MainPresenter(photoRepository: PhotoRepository, urlToPhotoService: UrlToPhotoService) : PhotoService.PhotoServiceCallback, RetrievePhotoFromURLTask.RetrievePhotoCallBack {

    private var photoRepository = photoRepository
    private var urlToPhotoService = urlToPhotoService
    private lateinit var display: Display

    //region async retrieve photo from url callbacks
    override fun onPhotoSuccessfullyRetrieved(bitmap: Bitmap) {
        display.showPhoto(bitmap)
    }

    override fun onFailToRetrievePhoto(message: String?) {
        message?.let {
            display.showErrorMessage(message)
        }
    }
    //endregion

    //region API callbacks
    override fun onSuccessPhotoServiceResponse(photo: Photo?) {
        photo?.let {
            if (photo.albumId == 1) {
                urlToPhotoService.retrieve(photo.url)
            }

        } ?: display.showErrorMessage("Could not retrieve Photo from path")
    }

    override fun onFailurePhotoServiceResponse(message: String) {
        display.showErrorMessage(message)
    }
    //endregion

    //region lifecycle
    fun inject(display: Display) {
        this.display = display
    }

    fun onStart() {
        urlToPhotoService.setRetrievePhotoCallBack(this)
        photoRepository.getPhotoFromRemote(this)

    }
    //endregion

    interface Display {
        fun showPhoto(bitmap: Bitmap)
        fun showErrorMessage(errorMessage: String)
    }
}