package com.pattern.mvp

import android.app.Application
import com.pattern.mvp.network.PhotoService
import com.pattern.mvp.network.RetrievePhotoFromURLTask
import com.pattern.mvp.network.UrlToPhotoService

class MVPApplication : Application() {
    private val photoService = PhotoService()
    private val urlToPhotoService = RetrievePhotoFromURLTask()

    fun getPhotoService(): PhotoService {
        return photoService
    }

    fun getUrlToPhotoService(): UrlToPhotoService {
        return urlToPhotoService
    }
}