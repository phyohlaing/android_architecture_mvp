package com.pattern.mvp

import android.graphics.Bitmap
import com.nhaarman.mockitokotlin2.*
import com.pattern.mvp.model.Photo
import com.pattern.mvp.network.PhotoRepository
import com.pattern.mvp.network.UrlToPhotoService
import com.pattern.mvp.presenter.MainPresenter
import org.junit.Test
import org.junit.Before

class MainPresenterTest {
    private val photoRepository = mock<PhotoRepository>()

    private val mockUrlToPhotoService: UrlToPhotoService = mock()

    private val mockDisplay: MainPresenter.Display = mock()

    private val mainPresenter = MainPresenter(photoRepository, mockUrlToPhotoService)


    private val mockPhoto = Photo(1, 11, "Test title", "google.com", "url.com")

    @Before
    fun injectMockDisplay() {
        mainPresenter.inject(mockDisplay)
    }

    @Test
    fun valid_response_from_api_retrieves_photo_from_path() {
        mainPresenter.onSuccessPhotoServiceResponse(mockPhoto)

        verify(mockUrlToPhotoService).retrieve("google.com")
    }

    @Test
    fun null_response_from_api_should_display_error_message() {
        mainPresenter.onSuccessPhotoServiceResponse(null)

        verify(mockDisplay).showErrorMessage("Could not retrieve Photo from path")
    }

    @Test
    fun error_response_from_api_should_display_error_message() {
        mainPresenter.onFailurePhotoServiceResponse("error message")

        verify(mockDisplay).showErrorMessage("error message")
    }

    @Test
    fun valid_response_when_retrieving_photo_from_path_displays_photo() {
        val mockPhoto: Bitmap = mock()
        mainPresenter.onPhotoSuccessfullyRetrieved(mockPhoto)

        verify(mockDisplay).showPhoto(mockPhoto)
    }

    @Test
    fun failure_when_retrieving_photo_from_path_displays_error_message_if_applicable() {
        mainPresenter.onFailToRetrievePhoto("error message")

        verify(mockDisplay).showErrorMessage("error message")
    }

    @Test
    fun failure_when_retrieving_photo_from_path_displays_nothing_if_error_message_is_empty() {
        mainPresenter.onFailToRetrievePhoto(null)

        verify(mockDisplay, never()).showErrorMessage("error message")
    }
}
